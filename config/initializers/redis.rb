REDIS_URL = URI.parse(ENV['REDISTOGO_URL'] || 'redis://localhost:6379/')
REDIS = Redis.new(host: REDIS_URL.host, port: REDIS_URL.port, password: REDIS_URL.password)

# REDIS_URL = URI.parse("redis://redistogo:53854627df24d6f64435344831958981@mummichog.redistogo.com:9340/" || 'redis://localhost:6379/' )
# REDIS = Redis.new(:host => REDIS_URL.host, :port => REDIS_URL.port, :password => REDIS_URL.password)

# clean stale players on restart. The websocket connections are gone anyway.
REDIS.del :players