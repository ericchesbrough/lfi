json.array!(@leaderboards) do |leaderboard|
  json.extract! leaderboard, :id, :game, :description, :date
  json.url leaderboard_url(leaderboard, format: :json)
end
