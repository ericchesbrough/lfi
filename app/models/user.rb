class User < ActiveRecord::Base
  belongs_to :leaderboard  
  validates :first_name, :last_name, :email,  presence: true

  # validates :tou, acceptance: true
end

