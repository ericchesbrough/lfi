class Question < ActiveRecord::Base

  def self.random
    offset = rand(Question.count)
    Question.offset(offset).first

    # offset_2 = rand(0...1.0)
    # Question.connection.execute "SELECT setseed(0.2})"
    # Question.order('random()').first
    # http://blog.bigbinary.com/2013/11/13/non-repeating-random-records.html
    # If question_id is the same as current question

  end

  def to_hash
    { id: id,
      question: question,
      answers: [{ answer: answer1, id: 1 },
                { answer: answer2, id: 2 },
                { answer: answer3, id: 3 },
                { answer: answer4, id: 4 }]
    }
  end
end
  


