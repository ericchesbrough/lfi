class Leaderboard < ActiveRecord::Base
  has_many :players

  def redis_key(str)
    "leaderboard:#{self.id}:#{str}"
  end
  
  def top_10_scores
    top_10_score_ids = $redis.zrevrange(self.redis_key('scores'), 0, 10)
    Player.find(top_10_score_ids).sort_by {|score| top_10_score_ids.index(score.id.to_s)}
  end




end





