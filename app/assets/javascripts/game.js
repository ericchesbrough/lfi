var username
var question_interval = 1
var countdown_timeout = 1000
var countdown_value
var countdown_interval = null
var letterValue = ["D", "C", "B", "A"]
var letterValuePlayerTwo = ["D", "C", "B", "A"]
var letterValueLeaderBoard = ["D", "C", "B", "A"]
var leaderboardAnswer = []

$(function() {

  function pulsate() {
    $(".find_out").
      animate({opacity: 0.2}, 1000, 'linear').
      animate({opacity: 1}, 1000, 'linear', pulsate);
  }
  pulsate();

  $('#lets_find_out').click(function (e) {
    $(".welcome_container").fadeOut(1100, function(){
      $(".sign_up_container").fadeIn(1000);
    });
    e.preventDefault()
  });
  $('.terms-of-use').click(function() {
    $(".sign_up_container").fadeOut(1100, function(){
      $(".legal-page-layout").fadeIn(1000);
    });
  });
  $('.accept-terms-button').click(function() {
    $(".legal-page-layout").fadeOut(1100, function(){
      $(".sign_up_container").fadeIn(1000);
    });
  });
});


function show_question(question) {
  if(question_interval != 6 ){
    clearLeaderBoard();
    grabAnswer();
    progress(10, 10, $('#progressBar'));
    $('.homepage').hide()
    $('.gamepage').show()
    $(".question_interval_number").html(question_interval)
    $('.game').hide()
    $('.question').html(question_interval + ".  " + question['question'])
    $('.question').attr('data-question-id', question['id'])
   
    // Player 1 Answer
    $('ul.answers').html('')

    $('ul#circled-answers').html('')
    $('ul#circled-answers-player-two').html('')

    shuffle(question['answers']).forEach(function (answer) {
      var finalLetter = letterValue.pop()
      $('ul.answers').append('<li class="listed-answer" letter-value="' + finalLetter + '" data-answer-id="' + answer['id'] + '">' + finalLetter + ".  " + answer['answer'] + '</li>')
      $('ul#circled-answers').append('<button class="answer" id="circled-answer" letter-value="' + finalLetter + '" data-answer-id="' + answer['id'] + '">' + finalLetter + '</button>')
    })
    
    // Player 2 Answer
    $('ul.player_two_answers').html('')
    shuffle(question['answers']).forEach(function (answer) {
      var finalLetterForPlayerTwo = letterValuePlayerTwo.pop();
      $('ul.player_two_answers').append('<li class="listed-answer" letter-value="' + finalLetterForPlayerTwo + '" data-answer-id="' + answer['id'] + '">' + finalLetterForPlayerTwo + ".  " + answer['answer'] + '</li>')
      $('ul#circled-answers-player-two').append('<button class="answer-two" id="circled-answer-two" letter-value="' + finalLetterForPlayerTwo + '" data-answer-id="' + answer['id'] + '">' + finalLetterForPlayerTwo + '</button>')

    })

    function toArray(array) {
      return array
    }

    function grabAnswer() {
      toArray(question['answers']).forEach(function (answer) {
        leaderboardAnswer.push(answer["answer"])
      })
      console.log(leaderboardAnswer)
      return leaderboardAnswer
    }

    function clearLeaderBoard() {
      return leaderboardAnswer = []
    }

    // Answers for Video Screen
    $('.question-container-right').html('')
    console.log(question["answers"])
    shuffle(question['answers']).forEach(function (answer) {
      var finalLetterLeaderBoard = letterValueLeaderBoard.pop()
        if (answer["answer"] == leaderboardAnswer[0].toString()) {
          $('.question-container-right').append('<div class="single-question group"><figure class="leaderboard-circle correct-answer-on-leaderboard">' + finalLetterLeaderBoard + '</figure>' + '<p class=" single-question-text" data-answer-id="' + answer['id'] + '">' + answer['answer'] + '</p></div>')
        } else {
          $('.question-container-right').append('<div class="single-question group"><figure class="leaderboard-circle ">' + finalLetterLeaderBoard + '</figure>' + '<p class=" single-question-text" data-answer-id="' + answer['id'] + '">' + answer['answer'] + '</p></div>')
        }
    })

    $('.game').fadeIn()
    question_interval++;
    letterValue.push("D")
    letterValue.push("C")
    letterValue.push("B")
    letterValue.push("A")
    letterValuePlayerTwo.push("D")
    letterValuePlayerTwo.push("C")
    letterValuePlayerTwo.push("B")
    letterValuePlayerTwo.push("A")
    letterValueLeaderBoard.push("D")
    letterValueLeaderBoard.push("C")
    letterValueLeaderBoard.push("B")
    letterValueLeaderBoard.push("A")
    
    // counts down every second via countdown_nextquestion function
    countdown_interval = setInterval(function(){countdown_nextquestion()}, 10);
  } 

}

function message_handler(msg) {
  console.log('Game Socket incoming:: ' + msg.data)
  message = JSON.parse(msg.data)
  if (message['players']) {
    // How do we differentiate 'name' that's coming in from websocket
    update_players(message['players'])
  } 
  else if (message['answer']){
    // stop_countdown()
    $(".total-score-player1").html(message['answer']["captured_score"])
    $('.player1-center').html(message['answer']['player_name'])
    $("#total-score-player1-leaderboard").html(message['answer']["captured_score"])
    if (message['answer']["correct"] == true){
      $(".leaderboard-lightbulb-left-"+message['answer']['question_interval']).addClass("leaderboard-lightbulb-left-"+message['answer']['question_interval']+"-success")

      $(".leaderboard-lightbulb-left-text-"+message['answer']['question_interval']).html(message['answer']['question_score'])
      // else score will be 0

      $('.fakebutton').css('background', 'green');
      $('.fakebutton').css('border', '4px solid green');
    } else if (message['answer']['correct'] == false) {
      $(".leaderboard-lightbulb-left-text-"+message['answer']['question_interval']).html("0")
      $('.fakebutton').css('background', 'red');
      $('.fakebutton').css('border', '4px solid red');
      $('.incorrect-notification').css('display', 'block');

    }
    show_answer(message['answer'])
  } 
  else if (message['question']) {
    stop_countdown()
    show_question(message['question'])
  } 
  else if (message['player_two_answer']) {
    // stop_countdown()
    $(".total-score-player2").html(message['player_two_answer']["captured_score_two"])
    $("#total-score-player2-leaderboard").html(message['player_two_answer']["captured_score_two"])
    $('.player2-center').html(message['player_two_answer']['player_name'])
    if (message['player_two_answer']["correct"] == true){
      $(".leaderboard-lightbulb-right-"+message['player_two_answer']['question_interval']).addClass("leaderboard-lightbulb-right-"+message['player_two_answer']['question_interval']+"-success")
      $(".leaderboard-lightbulb-right-text-"+message['player_two_answer']['question_interval']).html(message['player_two_answer']['question_score'])

      $('.fakebutton_two').css('background', 'green');
      $('.fakebutton_two').css('border', '4px solid green');
      $('.correct-notification-two').css('display', 'block');
    } else if (message['player_two_answer']['correct'] == false) {
      $(".leaderboard-lightbulb-right-text-"+message['player_two_answer']['question_interval']).html("0")
      $('.fakebutton_two').css('background', 'red');
      $('.fakebutton_two').css('border', '4px solid red');
      $('.incorrect-notification-two').css('display', 'block');

    }
    show_answer(message['player_two_answer'])
  }
  else if (message['next_question']) {
    stop_countdown()
    show_question(message['question'])
  }  
}

function logged_in() {
  first_initial =  $("#player_first_name").val().substr(0,1);
  last_name =  $("#player_last_name").val();
  email = $("#player_email").val();
  first_name_last_initial = first_initial + '.' + last_name
  username = first_name_last_initial
  socket_send('join', { name: username, email: email  })
}



function logged_out() {
  username = ''
  $('#player-name').show()
  $('#start_game').hide()
  $('.homepage').show()
  $('.gamepage').hide()
}


$(function() {
  $("#moderator_link").click(function(){
    $(".pregame").fadeOut(1000, function(){
      socket_close();
      socket_connect();
      socket.onmessage = message_handler;
      socket.onclose = logged_out;

      $(".leaderboard-container-section").fadeIn();
      $(".game").fadeIn();
      $(".playerboard").fadeIn();

      // setTimeout(startSocketForGame, 2000)

      // socket_send ('next_question')

      // $.get("/game/start");

    })
  });

// function startSocketForGame(){

//       $(".leaderboard-container-section").fadeIn();
//       $(".game").fadeIn();
//       $(".playerboard").fadeIn();
//   }
});




$(document).on('submit', '#new_player', function () {
  // These, below are for validations on the form.
  var first_name = $("#player_first_name").val();
  var last_name = $("#player_last_name").val();
  var email = $("#player_email").val();
  var company = $("#player_company").val();
  // JUST FIRST_NAME FOR TESTING. ADD ALL OTHER != ATTRIBUTES HERE BEFORE PROD.
  if (first_name == '') {
    $('#first-name-error').css('display', 'block');
  } else {
    $('#first-name-error').css('display', 'none');
  }

  if (last_name == '') {
    $('#last-name-error').css('display', 'block');
  } else {
    $('#last-name-error').css('display', 'none');
  }

  if (email == '') {
    $('#email-error').css('display', 'block');
  } else {
    $('#email-error').css('display', 'none');
  }

  // if (company == '') {
  //   $('#company-error').css('display', 'block');
  // } else {
  //   $('#company-error').css('display', 'none');
  // }

  if ( (first_name != '') && (last_name != '') && (email != '') ) {
  
  $(".sign_up_row").fadeOut()
    socket_close()
    socket_connect()
    socket.onopen = logged_in
    socket.onmessage = message_handler
    socket.onclose = logged_out
    $.ajax({
        url: $(this).attr('action'),
        type: $(this).attr('method'),
        data: $(this).serialize(),
        success: function() {
          $(".sign_up_container").fadeOut(1000, function(){
            $("#trivia_game").fadeIn(900);
          });
        }
    });
  }
  return false
})


$(document).on('click', '#circled-answer', function () {
  $(this).css('background', '#009ACD');
  $(this).css('color', '#fff');
})


$(document).on('click', '#circled-answer-two', function () {
  $(this).css('background', '#009ACD');
  $(this).css('color', '#fff');
})

$(document).on('click', '.answer', function () {

  $('#circled-answers > .answer').not(this).css('display', 'none')
  $(this).addClass('fakebutton')
  $(this).removeClass('answer')


  socket_send('answer', { question_id: $('.question').attr('data-question-id'),
  answer_id: $(this).data('answer-id'), captured_score: $('.countdown').html(), question_interval: question_interval })
})


$(document).on('click', '.answer-two', function () {

  $('#circled-answers-player-two > .answer-two').not(this).css('display', 'none')
  $(this).addClass('fakebutton_two')
  $(this).removeClass('answer-two')


  socket_send('player_two_answer', { question_id: $('.question').attr('data-question-id'),
  answer_id: $(this).data('answer-id'), captured_score_two: $('.countdown').html(), question_interval: question_interval })

})




function update_players(players) {
  $('ul.players-list').html('')
  Object.keys(message['players']).forEach(function (player_name) {
    $('ul.players-list').append('<li>' + player_name + ' (' + message['players'][player_name]['score'] + ')</li>')
    $(".score").html(message['players'][username]['score'])
    $(".username_span").html(message['players'][username]['name'])
  })
}


function stop_countdown() {
  // reset first
  countdown_value = countdown_timeout + 1; // seconds to change to next question + 1 to start in the right number
  $(".countdown").html(countdown_timeout);
  clearInterval(countdown_interval); 
}


function prepTimer() {
  var seconds;
  var temp;
 
  function countdown() {
    seconds = document.getElementById('countdown_prep').innerHTML;
    seconds = parseInt(seconds, 10);
 
    if (seconds == 1) {
      temp = document.getElementById('countdown_prep');
      $('.prep-for-next-question').hide();
        socket_send('next_question', { question_id: $('.question').attr('data-question-id'),
        answer_id: null})
      return;
    }
 
    seconds--;
    temp = document.getElementById('countdown_prep');
    temp.innerHTML = seconds;
    timeout = setTimeout(countdown, 1000);
  } 
 
  countdown();
}

function nextScreen() {
  var seconds;
  var temp;
 
  function countdownTwo() {
    seconds = document.getElementById('next_screen').innerHTML;
    seconds = parseInt(seconds, 10);
 
    if (seconds == 1) {
      temp = document.getElementById('next_screen');
      $('.prep-for-next-question-two').hide();
      return;
    }
 
    seconds--;
    temp = document.getElementById('next_screen');
    temp.innerHTML = seconds;
    timeout = setTimeout(countdownTwo, 1000);
  } 
 
  countdownTwo();
}

function showLeaderBoardAnswer() {
  $('.correct-answer-on-leaderboard').css('background', 'green')
  $('.correct-answer-on-leaderboard').css('border', '2px solid green')
}

function countdown_nextquestion() {
  $('#countdown_prep').html('8');
  $('#next_screen').html('7');
  countdown_value--;
  $(".countdown").html(countdown_value);
  if(countdown_value == 0) {
    showLeaderBoardAnswer();
    stop_countdown()
    // change to next question
    if (question_interval <= 5) {
      $(".gamepage").fadeOut(1000, function(){
        $(".prep-for-next-question").fadeIn(1200, function(){
            prepTimer();
          $('.correct-notification').css('display', 'none')
          $('.incorrect-notification').css('display', 'none')
        });
        $(".prep-for-next-question-two").fadeIn(1200, function(){
            nextScreen();
          $('.correct-notification-two').css('display', 'none')
          $('.incorrect-notification-two').css('display', 'none')
        });
      });
    } 
    else {
      $(".end_of_game").fadeOut(1000, function(){
        $(".thanks").fadeIn(1000, function(){
          $(".thanks").fadeOut(5000, function(){
            // redirect for player
             window.location.href = "http://lfi.herokuapp.com/"
            // window.location.href = "http://localhost:3000/"
          });
        });
        $(".thanks_player_two").fadeIn(1000, function(){
          $(".thanks_player_two").fadeOut(5000, function(){
          // redirect for player
          window.location.href = "http://lfi.herokuapp.com/player_two/"
          // window.location.href = "http://localhost:3000/player_two"
          });
        });
        $(".new_game").fadeIn(1000, function(){ 
          $(".new_game").fadeOut(25000, function(){
            // redirect for player
            location.reload(true);
            // window.location.href = "http://localhost:3000/leaderboard"
          });
        });
      });
    }
  } 
}



function progress(timeleft, timetotal, $element) {
    var progressBarWidth = timeleft * $element.width() / timetotal;
    $element.find('div').animate({ width: progressBarWidth }, timeleft == timetotal ? 0 : 1000, 'linear');
    if(timeleft > 0) {
        setTimeout(function() {
            progress(timeleft - 1, timetotal, $element);
        }, 1000);
    }
};


function show_answer(answer) {
  // this is where we get back information about each Player's answer
  // answer_id is referring to data-answer-id
  console.log('Answer incoming: ' + answer['answer_id'] + ' ' + answer['player_name'] + ' ' + answer['correct'] + 'question #:' + question_interval)
}


function shuffle(array) {
  return array.sort(function (a,b) {
    return (a['answer'].length - b['answer'].length)
  })
}



