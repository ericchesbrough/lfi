class Admin::BaseController < ApplicationController
  http_basic_authenticate_with name: ENV['admin_name'] || 'scc', password: ENV['admin_password'] || 'password'

  def index
  end
end
