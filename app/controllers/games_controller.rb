class GamesController < ApplicationController
  include Tubesock::Hijack

  def new
    # @user = User.new
  end

  def socket
    hijack do |tubesock|

      player = Player.new

      # Outbound types:
      # players: []
      # question: { id: '', question: '', answers: [] }
      # chat: { sender: '', message: '' }

      # Each player is using his own redis thread with extra redis connection
      redis_thread = Thread.new do
        redis = Redis.new(host: REDIS_URL.host, port: REDIS_URL.port, password: REDIS_URL.password)
        redis.subscribe 'game' do |on|
          on.message do |_channel, message| 
            logger.debug "Outgoing websocket to #{player.name} (#{player.uid}): #{message}"
            tubesock.send_data message
          end
        end
      end

      # Inbound types:
      # :join, content: { name: '' }
      # :chat, content: { message: '' }
      # :answer, content: { question_id: '', answer_id: '' }

      tubesock.onmessage do |message|

        message = JSON.parse(message, symbolize_names: true)

        logger.debug "Incoming websocket from #{player.name} (#{player.uid}): #{message}"
        next unless message[:type] && message[:content].is_a?(Hash)
        type, content = message[:type].to_sym, message[:content]

        # player = Player.find_by_email(content[:email])
        
        case type

        when :join
          # This, below, works on :join because of logged_in function in game.js
          player.name = content[:name]
          player.email = content[:email]
          player.save
          REDIS.publish 'game', Player.players_socket_json
          REDIS.publish 'game', {
            players: { name: content[:name] },
          }.to_json
        when :answer
          question = Question.find(content[:question_id])
          # correct_answer = question.answer1
          correct = question.solution == content[:answer_id].to_i
          updated_score = content[:captured_score].to_i
          if correct
            question_score = content[:captured_score]
            player.score += updated_score
            logger.debug "Correct answer to question #{question.id} from: #{player.name} (#{player.uid})"
          else
            question_score = 0
            player.score -= 0
            logger.debug "Wrong answer to question #{question.id} from: #{player.name} (#{player.uid})"
          end
          player.save
          REDIS.publish 'game', Player.players_socket_json
          REDIS.publish 'game', {
            answer: { answer_id: content[:answer_id], player_name: player.name, correct: correct, player_email: player.email, captured_score: player.score, question_interval: content[:question_interval], question_score: question_score },
          }.to_json
          

        when :player_two_answer
          question = Question.find(content[:question_id])
          correct = question.solution == content[:answer_id].to_i
          updated_score_two = content[:captured_score_two].to_i
          if correct
            question_score = content[:captured_score_two]
            player.score += updated_score_two
            logger.debug "Correct answer to question #{question.id} from: #{player.name} (#{player.uid})"
          else
            question_score = 0
            player.score -= 0
            logger.debug "Wrong answer to question #{question.id} from: #{player.name} (#{player.uid})"
          end
          player.save
          REDIS.publish 'game', Player.players_socket_json
          REDIS.publish 'game', {
            player_two_answer: { answer_id: content[:answer_id], player_name: player.name, correct: correct, player_email: player.email, captured_score_two: player.score, question_interval: content[:question_interval], question_score: question_score},
          }.to_json
          
        when :next_question
          REDIS.publish 'game', { question: Question.random.to_hash }.to_json

        # when :initial_question
        #   REDIS.publish 'game', { question: Question.random.to_hash }.to_json
        #   render(nothing: true)
          
        else
          logger.debug "Unhandled socket message type: #{type}"
          # tubesock.send_data 'direct'
          REDIS.publish 'game', { relay: message }.to_json
        end
      end

      # stop redis subscriber when client leaves
      tubesock.onclose do
        logger.debug "Socket close from: #{player.name} (#{player.uid})"
       if player.email != nil
          p = Player.where(email:player.email).last
          p.score = player.score
          p.save!

        end
          player.destroy
          REDIS.publish 'game', Player.players_socket_json
          redis_thread.kill
      end
    end
  end

  def start
    REDIS.publish 'game', { question: Question.random.to_hash }.to_json
    render(nothing: true)
  end

  private

  def player_params
    params.require(:player).permit(:first_name, :last_name, :email, :company, :tou, :uid, :name, :score)
  end


end
