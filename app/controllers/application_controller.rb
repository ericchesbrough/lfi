class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  skip_before_filter :verify_authenticity_token

  def index
    @players = REDIS.smembers(:players).map { |p| JSON.parse(p) }
    @player = Player.new
    render :welcome
  end

  def player_two
    @players = REDIS.smembers(:players).map { |p| JSON.parse(p) }
    @player = Player.new
    render :player_two
  end


end


