class MakePlayerAnActiveRecordModel < ActiveRecord::Migration
  def change
    create_table :players do |t|
      t.string :name
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :company
      t.boolean :tou
      t.integer :uid
      t.integer :score
      t.timestamps
    end
  end
end
