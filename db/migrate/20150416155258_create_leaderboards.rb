class CreateLeaderboards < ActiveRecord::Migration
  def change
    create_table :leaderboards do |t|
      t.string :game
      t.string :description
      t.string :date

      t.timestamps
    end
  end
end
