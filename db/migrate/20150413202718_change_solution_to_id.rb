class ChangeSolutionToId < ActiveRecord::Migration
  def change
    remove_column :questions, :solution, :string
    add_column :questions, :solution, :integer
  end
end
