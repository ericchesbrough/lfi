# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Player.delete_all
Question.delete_all

seeds = YAML.load(File.open(File.join(Rails.root, 'config', 'questions.yml')))


seeds['questions'].each do  |question|
  Question.find_or_create_by(question: question['question'],
                  answer1: question['answer1'],
                  answer2: question['answer2'],
                  answer3: question['answer3'],
                  answer4: question['answer4'],
                  solution: question['solution'])
end

Player.create!(first_name:"Jason", last_name: "Samuelian", score: 3232, email: "jasom.samueliam@fsgi.com")
Player.create!(first_name:"Nick", last_name: "Hill", score: 2747, email: "nhill@hill-energy.com")
Player.create!(first_name:"Daniel", last_name: "Hammerman", score: 2465, email: "drhammerman@verizon.net")
Player.create!(first_name:"Rick", last_name: "Ekelof", score: 2289, email: "rekelof@sccadv.com")
Player.create!(first_name:"Robert", last_name: "Snyder", score: 2033, email: "rsnyder@rworldenergy.com")
Player.create!(first_name:"John", last_name: "Ortiz", score: 1925, email: "john.ortiz@fsgi.com")
Player.create!(first_name:"Mary", last_name: "McGrath", score: 1906, email: "mmcgrath@carltonpc.com")
Player.create!(first_name:"Tom", last_name: "Carlins", score: 1849, email: "tcarlins@carlinsconsulting.com")
Player.create!(first_name:"D", last_name: "H", score: 1815, email: "doro@gehr.com")
Player.create!(first_name:"Frank", last_name: "Messina", score: 1752, email: "ischiafrank@gmail.com")